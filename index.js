const req = require("request");
const http = require("http");

const {
    JSDOM
} = require("jsdom");
const emailSender = require("./sendmail");
const fs = require("fs");

// from to help
// tehran 1
// abade 337
// shz 255

//sex 1 brothers, 2 sisters 3 both
function checkTrain(date, from, to, sex, wagon) {
    let form1 = {
        way: 1,
        fromd: date,
        tod: date,
        from: from,
        to: to,
        sex: sex,
        wagon: wagon,
        passCnt: 1
    };
    req.post({
            url: "https://payaneh.ir/etrain/searchWagn.php",
            form: form1
        },
        (err, res, body) => {
            if (err) {
                console.error(err);
                return;
            }
            // body = fs.readFileSync('./r.html');
            const doc = new JSDOM(body);
            let items = doc.window.document.querySelectorAll(
                ".browserdisable td:nth-child(10)"
            );
            // console.log(items);
            if (items == null) {
                return;
            }
            for (let index = 0; index < items.length; index++) {
                const element = items[index];
                let numberOfFree = element.textContent;
                // console.log(`numberOfFree: ${numberOfFree}, date:${date} `)
                if (Number(numberOfFree) > 0) {
                    emailSender.sendMail(
                        `Found ${numberOfFree} free ticket on ${date}!!!, Hurry Up Now\n ${body}`
                    );
                }
                // console.log(`Time ${Date().toString()}, count: ${nums}`);
            }
        }
    );
}

function checkBus(date) {
    let dateString = date.split("/").join("");
    let url = `https://payaneh.ir/CRS/start.php?sourceCity=11321006&desCity=41320000&date=${dateString}&cnt=1&company=0`;
    req.get({
            url
        },
        (err, res, body) => {
            // body = fs.readFileSync('./busexample.html');
            const doc = new JSDOM(body);
            let items = doc.window.document.querySelectorAll(
                "tr.col1 td:nth-child(9),tr.col2 td:nth-child(9)"
            );
            // console.log(items);
            if (items == null) {
                return;
            }
            let found = false;
            for (let index = 0; index < items.length; index++) {
                const element = items[index];
                let numberOfFree = element.textContent;
                // console.log(`numberOfFree: ${numberOfFree}, date:${date} `)
                if (Number(numberOfFree) > 0) {
                    console.log(`Bus found on ${date}`);
                    found = true;
                }
            }
            if (found) {
                emailSender.sendMail(
                    `Found ${numberOfFree} free buss ticket on ${date}!!!, Hurry Up Now\n ${body}`
                );
            }
        }
    );
}

function checkDates() {
    try {
        // checkTrain("1397/12/26", "1", "255", 1, 0);
        // checkTrain("1397/12/27", "1", "255", 1, 0);
        // checkTrain("1397/12/28", "1", "255", 1, 0);
        // checkTrain("1397/12/29", "1", "255", 1, 0);

        // checkTrain("1397/12/26", "1", "255", 3, 0);
        // checkTrain("1397/12/27", "1", "255", 3, 0);
        // checkTrain("1397/12/28", "1", "255", 3, 0);
        // checkTrain("1397/12/29", "1", "255", 3, 0);

        checkTrain("1398/01/09", "337", "1", 3, 1);
        checkTrain("1398/01/10", "337", "1", 3, 1);
        checkTrain("1398/01/11", "337", "1", 3, 1);
        checkTrain("1398/01/12", "337", "1", 3, 1);

        // checkBus("1397/12/26");
        // checkBus("1397/12/27");
        // checkBus("1397/12/28");
        // checkBus("1397/12/29");
    } catch (error) {
        console.error(error);
    }
}

console.log("staring");
console.log(`process.env.PORT ${process.env.PORT}`);

http
    .createServer((req, res) => {
        res.write(`Im Here!`);
        res.end();
    })
    .listen(process.env.PORT || 3000);

checkDates();

setInterval(() => {
    checkDates();
}, 10 * 60 * 1000);

setInterval(() => {
    emailSender.sendMail(`Heyyyy, I'm still up and searching for you ;)`);
}, 24 * 60 * 60 * 1000);